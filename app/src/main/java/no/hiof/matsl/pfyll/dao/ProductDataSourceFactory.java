package no.hiof.matsl.pfyll.dao;

import android.arch.paging.DataSource;
import android.support.annotation.NonNull;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

import no.hiof.matsl.pfyll.util.Filter;
import no.hiof.matsl.pfyll.model.FirestoreProduct;
import no.hiof.matsl.pfyll.util.IdFilter;

/*
 * Creates a ProductDataSource given a Firestore database and a list of filters.
 */
public class ProductDataSourceFactory extends DataSource.Factory<DocumentSnapshot, FirestoreProduct> {

    private FirebaseFirestore database;
    private List<Filter> filters;
    private IdFilter idFilter;

    public ProductDataSourceFactory(@NonNull FirebaseFirestore database) {
        this.database = database;
    }

    /*
     * Constructor for an id-filtered factory.
     */
    public ProductDataSourceFactory(@NonNull FirebaseFirestore database, @NonNull IdFilter idFilter) {
        this(database);
        this.idFilter = idFilter;
    }
    /*
     * Constructor for a value-filtered factory.
     */
    public ProductDataSourceFactory (@NonNull FirebaseFirestore database,  @NonNull List<Filter> filters) {
        this(database);
        this.filters = filters;
    }

    @Override
    public DataSource<DocumentSnapshot, FirestoreProduct> create() {
        // An IdFilter overrides any other filters
        if (idFilter != null)
            return new ProductDataSource(database, idFilter);
        else
            return new ProductDataSource(database, filters);
    }
}
