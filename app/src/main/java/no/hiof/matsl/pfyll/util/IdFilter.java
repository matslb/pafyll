package no.hiof.matsl.pfyll.util;

import java.util.List;

public class IdFilter {
    private List<String> ids;

    public IdFilter(List<String> ids) {
        this.ids = ids;
    }

    public List<String> getIds() {
        return ids;
    }
}
